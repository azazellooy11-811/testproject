﻿using System;
using Clinic.Core.Models;
using Clinic.Database;
using Clinic.Database.Entities;
using Microsoft.EntityFrameworkCore;

namespace Clinic.Core.Services
{
    public interface IAnimalHostService
    {
        Task AddAnimalHost(string firstname, string lastname, AnimalModel animals);
      
        Task<bool> Update(long animalHostId, string firstname, string lastname, long animId, string nickname, int age, string vaccination);
    }
    public class AnimalHostService : IAnimalHostService
    {
        private readonly DoctorContext _doctorContext;

        public AnimalHostService(DoctorContext doctorContext)
        {
            _doctorContext = doctorContext;
        }
        private async void generateClinicData(AnimalHost animalHost)
        {
            var veterinarCount = _doctorContext.Veterinarians.Count();
            var rnd = new Random();
            var rndCount = rnd.Next(1, veterinarCount);
            var veterinar = _doctorContext.Veterinarians.FirstOrDefault(x =>x.Id == rndCount);
            var orderCount = _doctorContext.Clinics.Count();
            int orderNumber = orderCount+1;
            var clinic = new Clinics
            {
                OrderNumber = orderNumber,
                Veterinarians = veterinar,
                AnimalHosts = animalHost
            };
            await _doctorContext.Clinics.AddRangeAsync(clinic);

        }
        public async Task AddAnimalHost(string firstname, string lastname, AnimalModel animals)
        {
            var animal = new Animal
            {
                Nickname = animals.Nickname,
                Age = animals.Age,
                Vaccinations = animals.Vaccinations
            };
            var animalHost = new AnimalHost
            {

                FirstName = firstname,
                LastName = lastname,
                Animals = animal
            };
            generateClinicData(animalHost);
            await _doctorContext.AnimalHosts.AddRangeAsync(animalHost);
            await _doctorContext.Animals.AddRangeAsync(animal);
            await _doctorContext.SaveChangesAsync();
        }

        public async Task<bool> Update(long animalHostId, string firstname, string lastname, long animId, string nickname, int age, string vaccination)
        {
            var client = await _doctorContext.AnimalHosts.FirstOrDefaultAsync(x =>
                x.Id == animalHostId);

            if (firstname!= null)
                client.FirstName = firstname;
            if (lastname!= null)
                client.LastName = lastname;
            if (nickname != null)
                client.Animals.Nickname = nickname;
            if (animId != null)
                client.Animals.Id = animId;
            if (age!= null)
                client.Animals.Age = age;
            if (vaccination!= null)
                client.Animals.Vaccinations = vaccination;

            _doctorContext.AnimalHosts.Update(client);
            _doctorContext.Animals.Update(client.Animals);
            await _doctorContext.SaveChangesAsync();
            return true;
        }
    }
}

