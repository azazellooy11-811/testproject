﻿using System;
using Clinic.Database;
using Clinic.Database.Entities;

namespace Clinic.Core.Services
{
    public interface IVeterinarianService
    {
        Task AddVeterinarian(string doctorName, string speciality);
    }
    public class VeterinarianService : IVeterinarianService
    {
        private readonly DoctorContext _doctorContext;

        public VeterinarianService(DoctorContext doctorContext)
        {
            _doctorContext = doctorContext;
        }

        public async Task AddVeterinarian(string doctorName, string speciality)
        {
            var veterinarian = new Veterinarian
            {
                DoctorName = doctorName,
                Speciality = speciality
            };
            await _doctorContext.Veterinarians.AddRangeAsync(veterinarian);
            await _doctorContext.SaveChangesAsync();
        }

    }
}

