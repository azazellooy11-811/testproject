﻿using System;
using Clinic.Database;
using Clinic.Database.Entities;
using Microsoft.EntityFrameworkCore;

namespace Clinic.Core.Services
{
    public interface IClinicService
    {
        List<Clinics> GetAll();
    }
    public class ClinicService : IClinicService
    {
        private readonly DoctorContext _doctorContext;

        public ClinicService(DoctorContext doctorContext)
        {
            _doctorContext = doctorContext;
        }

        public List<Clinics> GetAll()
        {
            var result = _doctorContext.Clinics.ToList();
            return result;
        }

    }
}

