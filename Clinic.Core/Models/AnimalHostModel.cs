﻿using System;
using System.ComponentModel.DataAnnotations;
using Clinic.Database.Entities;

namespace Clinic.Core.Models
{
    public class AnimalHostModel
    {
        [Required] public string FirstName { get; set; }
        [Required] public string LastName { get; set; }
    }
}

