﻿using System;
using Clinic.Database.Entities;

namespace Clinic.Core.Models
{
    public class ClinicModel
    {
        public int OrderNumber { get; set; }
        public Veterinarian Veterinarians { get; set; } 
        public AnimalHost AnimalHosts { get; set; } 
    }
}

