﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Clinic.Core.Models
{
    public class VeterinarianModel
    {
        [Required] public string DoctorName { get; set; }
        [Required] public string Speciality { get; set; }
    }
}

