﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Clinic.Core.Models
{
    public class AnimalModel
    {
        [Required] public string Nickname { get; set; }
        [Required] public int Age { get; set; }
        [Required] public string Vaccinations { get; set; }
    }
}

