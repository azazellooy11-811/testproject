﻿using System;
using Clinic.Database.Entities;

namespace Clinic.Core.Models
{
    public class ClinicDto
    {
        public long Id { get; set; }
        public int OrderNumber { get; set; }
        public Veterinarian Veterinarians { get; set; }
        public AnimalHost AnimalHosts { get; set; }

        public static ClinicDto GetClinicDto(Clinics clinic,Veterinarian veterinarian, AnimalHost animalHost)
        {
            return new ClinicDto
            {
                Id = clinic.Id,
                OrderNumber = clinic.OrderNumber,
                Veterinarians = veterinarian,
                AnimalHosts = animalHost
            };
        }
    }
}

