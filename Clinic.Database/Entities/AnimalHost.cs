﻿using System;
namespace Clinic.Database.Entities
{
    public class AnimalHost
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Animal Animals { get; set; } = new Animal();
        
    }
}

