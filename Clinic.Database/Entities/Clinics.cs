﻿using System;
namespace Clinic.Database.Entities
{
    public class Clinics
    {
       public long Id { get; set; }
       public int OrderNumber { get; set; }
       public Veterinarian Veterinarians { get; set; } = new Veterinarian();
       public AnimalHost AnimalHosts { get; set; } = new AnimalHost(); 
    }
}

