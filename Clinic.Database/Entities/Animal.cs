﻿using System;

namespace Clinic.Database.Entities
{
    public class Animal
    {
        public long Id { get; set; }
        public string Nickname { get; set;}
        public int Age { get; set; }
        public string Vaccinations { get; set; }
    }
}

