﻿using System;
namespace Clinic.Database.Entities
{
    public class Veterinarian
    {
        public long Id { get; set; }
        public string DoctorName { get; set; }
        public string Speciality { get; set; }
    }
}

