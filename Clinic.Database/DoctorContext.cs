﻿using Microsoft.EntityFrameworkCore;
using Clinic.Database.Entities;

namespace Clinic.Database
{
    public class DoctorContext: DbContext
    {
        public DbSet<Animal> Animals { get; set; }

        public DbSet<AnimalHost> AnimalHosts { get; set; }

        public DbSet<Veterinarian> Veterinarians { get; set; }
        public DbSet<Clinics> Clinics { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)

        {

            optionsBuilder.UseNpgsql(

                "Host=localhost;Port=5432;Database=Doctor;Username=postgres;Password=postgres;");

        }

    }
}

