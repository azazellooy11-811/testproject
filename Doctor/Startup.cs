﻿using System;
using Clinic.Core.Services;
using Clinic.Database;
using Doctor.Core;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;


namespace Doctor
{
        public class Startup
        {
            public Startup(IConfiguration configuration)
            {
                Configuration = configuration;
            }

            public IConfiguration Configuration { get; }

            public void ConfigureServices(IServiceCollection services)
            {
                services.AddCors();
                services.AddControllers();

                services.AddSwaggerGen(c =>
                {
                    c.SwaggerDoc("v1", new OpenApiInfo
                    {
                        Title = "API",
                        Version = "v1"
                    });
                    c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                    {
                        In = ParameterLocation.Header,
                        Description = "Please insert JWT with Bearer into field",
                        Name = "Authorization",
                        Type = SecuritySchemeType.ApiKey
                    });
                    c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        new string[] { }
                    }
                });
                });

                services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                    .AddCookie(options =>
                    {
                        options.LoginPath = new PathString("/Login/Index");
                        options.AccessDeniedPath = new PathString("/Login/Login");
                        options.ExpireTimeSpan = new TimeSpan(14, 0, 0);
                    });
                services.AddBusinessLogicLayerDI();
                services.AddDbContext<DoctorContext>();


                services.ConfigureApplicationCookie(options =>
                {
                    options.Cookie.SameSite = SameSiteMode.None;
                    options.LoginPath = "/";
                    options.ExpireTimeSpan = TimeSpan.FromDays(365);
                    options.Cookie.IsEssential = true;
                });


                services.Configure<ForwardedHeadersOptions>(options =>
                {
                    options.ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
                    options.KnownNetworks.Clear();
                    options.KnownProxies.Clear();
                });
                services.AddScoped<IAnimalHostService, AnimalHostService>(); 
                services.AddScoped<IClinicService, ClinicService>();
                services.AddScoped<IVeterinarianService, VeterinarianService>();
        }

            public void Configure(IApplicationBuilder app, IWebHostEnvironment env, DoctorContext doctorContext)
            {
                AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "Expert API"); });
                app.UseStaticFiles();
                app.UseRouting();
                doctorContext.Database.Migrate();
                // global cors policy
                app.UseCors(x => x
                    .SetIsOriginAllowed(origin => true)
                    .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials());

                app.UseAuthentication();
                app.UseAuthorization();

                app.UseEndpoints(x => x.MapControllers());
            }
        }
}    

